import json
import logging
import os

from logging.handlers import TimedRotatingFileHandler

_d = os.path.dirname
_ROOT_FOLDER = os.path.join(_d(_d(os.path.abspath(__file__))))


class Singleton(type):
    """
    Define an Instance operation that lets clients access its unique
    instance.
    """

    def __init__(cls, name, bases, attrs, **kwargs):
        super().__init__(name, bases, attrs)
        cls._instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__call__(*args, **kwargs)
        return cls._instance


class Configuration(object, metaclass=Singleton):
    def __init__(self):
        self.configFilePath = _ROOT_FOLDER + '\Config\config.json'
        self.applicationServerPath = None
        self.tempPath = None
        self.logPath = None
        self.serverPort = None
        self.serverIpAddress = None
        self.debug = None
        self.environmentName = None
        self.mode = None
        # Logger
        logger = None
        self._initLogging()
        # Locking mechanism
        self.uid = None
        # Authentication True enabled / False disable
        self.authentication = None
        # Admin password
        self.adminPassword = None

        data = None
        try:
            data = json.load(open(self.configFilePath))
        except IOError:
            print('File config.json has been not found .... ')
            print('Looking for file in folder:', self.configFilePath)
            quit()

        # TODO refactor check against existing list and make just one if / else
        # check on applicationServerPath key
        if 'applicationServerPath' in data:
            self.setApplicationServerPath(data['applicationServerPath'])
        else:
            print('Error: missing configuration key wlpPath')
            quit()

        # check on tempPath key
        if 'tempPath' in data:
            self.setTempPath(data['tempPath'])
        else:
            print('Error: missing configuration key tempPath')
            quit()

        # check on serverPort key
        if 'serverPort' in data:
            self.setServerPort(data['serverPort'])
        else:
            print('Error: missing configuration key serverPort')
            quit()

        # check on serverIpAddress key
        if 'serverIpAddress' in data:
            self.setServerIpAddress(data['serverIpAddress'])
        else:
            print('Error: missing configuration key serverIpAddress')
            quit()

        # check on debug key
        if 'debug' in data:
            self.setDebug(data['debug'])
        else:
            print('Error: missing configuration key serverPort')
            quit()

        # check on environmentName key
        if 'environmentName' in data:
            self.setEnvironmentName(data['environmentName'])
        else:
            print('Error: missing configuration key environmentName')
            quit()

        # check on mode key
        if 'mode' in data:
            self.setMode(data['mode'])
        else:
            print('Error: missing configuration key mode')
            quit()

        # check on authentication key
        if 'authentication' in data:
            self.setAuthentication(data['authentication'])
        else:
            print('Error: missing configuration key authentication')
            quit()

        # check on adminPassword key
        if 'adminPassword' in data:
            self.setAdminPassword(data['adminPassword'])
        else:
            print('Error: missing configuration key adminPassword')
            quit()

    # Getters
    def getApplicationServerPath(self):
        return self.applicationServerPath

    def getTempPath(self):
        return self.tempPath

    def getServerPort(self):
        return self.serverPort

    def getDebug(self):
        return self.debug

    def getServerIpAddress(self):
        return self.serverIpAddress

    def getEnvironmentName(self):
        return self.environmentName

    def getMode(self):
        return self.mode

    def getLockUid(self):
        return self.uid

    def getAuthentication(self):
        return self.authentication

    def getAdminPassword(self):
        return self.adminPassword

    # Setters
    def setApplicationServerPath(self, value):
        self.applicationServerPath = value

    def setTempPath(self, value):
        self.tempPath = value

    def setServerPort(self, value):
        self.serverPort = value

    def setServerIpAddress(self, value):
        self.serverIpAddress = value

    def setDebug(self, value):
        self.debug = value

    def setEnvironmentName(self, value):
        self.environmentName = value

    def setMode(self, value):
        self.mode = value

    def setDebugMessage(self, value):
        if "True" in self.debug:
            self.logger.debug(value)

    def setInfoMessage(self, value):
        self.logger.info(value)

    def setErrorMessage(self, value):
        self.logger.error(value)

    def setWarningMessage(self, value):
        self.logger.warning(value)

    def setLockUid(self, value):
        self.uid = value

    def setAuthentication(self, value):
        self.authentication = value

    def setAdminPassword(self, value):
        self.adminPassword = value

    def _initLogging(self):
        self.logger = logging.getLogger('deployer')
        # Setup Handler one for file and one for stream
        dirPath = _ROOT_FOLDER + '/Logs/'
        if not os.path.isdir(dirPath):
            os.makedirs(dirPath)

        path = dirPath + 'deployer.log'
        # Set Handlers
        fileHandler = TimedRotatingFileHandler(path, when="d", interval=1, backupCount=5)
        handler = logging.StreamHandler()

        # Set Formatters
        formatter = logging.Formatter(
            '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')

        handler.setFormatter(formatter)
        fileHandler.setFormatter(formatter)
        #
        self.logger.addHandler(handler)
        self.logger.addHandler(fileHandler)
        self.logger.setLevel(logging.DEBUG)
