import os
import shutil
import subprocess
import time

from Models.Command import Command
from Models.Configuration import Configuration
from Models.DB import DB, DbException

c = Configuration()


class ServerToolsWlp(object):
    _SERVERS_DIR = c.getApplicationServerPath() + "/usr/servers/"
    _CREATE_COMMAND = c.getApplicationServerPath() + "/bin/server.bat create "
    _STATUS_COMMAND = c.getApplicationServerPath() + "/bin/server.bat status "
    _STOP_COMMAND = c.getApplicationServerPath() + "/bin/server.bat stop "
    _START_COMMAND = c.getApplicationServerPath() + "/bin/server.bat start "

    def startServer(self, serverName):
        """
        Try to start server
        """
        # TODO add maybe check on ports here ? if ports are taken throw exception
        if self.isServerExist(serverName):
            cmd = Command(self._START_COMMAND + serverName)
            cmd.run(timeout=10, shell=False, universal_newlines=True, stdout=subprocess.DEVNULL,
                    stderr=subprocess.STDOUT)

            c.setDebugMessage('{startServer} cmd status: ' + str(cmd.status))
            c.setDebugMessage('{startServer} cmd output: ' + str(cmd.output))
            c.setDebugMessage('{startServer} cmd error: ' + str(cmd.error))

            if "ISR" in self.getServerStatus(serverName):
                c.setDebugMessage('{startServer} Server ' + serverName + ' has been started')
                try:
                    query = "UPDATE servers SET status=\'{}\' WHERE serverName=\'{}\';".format('1', serverName)
                    DB(statements=query)
                    c.setInfoMessage(
                        '{startServer} Server ' + serverName + ' has been updated to status 1')
                except DbException:
                    c.setErrorMessage(
                        '{startServer} Server ' + serverName + ' cannot be update to status 1')
            else:
                c.setWarningMessage(
                    '{startServer} Server ' + serverName + ' cannot be started. Server status is: ' + self.getServerStatus(
                        serverName))

    def deleteServer(self, serverName):
        """
        Try to delete server
        """
        if self.isServerExist(serverName):
            self.stopServer(serverName)
            # Do wait until server is stopped

            while 'INR' not in self.getServerStatus(serverName):
                c.setDebugMessage('{deleteServer} Waiting until server is completely stopped ...')
                time.sleep(1)

            # Delete serverName from database
            try:
                query = "DELETE FROM servers WHERE serverName=\'{}\';".format(serverName)
                DB(statements=query)
                # Delete
                if self.isServerExist(serverName):
                    shutil.rmtree(self._SERVERS_DIR + serverName)

                c.setInfoMessage('{deleteServer} Server ' + serverName + ' has been removed from database')
            except DbException:
                c.setErrorMessage('{deleteServer} Server ' + serverName + ' cannot be deleted from database')

    def stopServer(self, serverName):
        """
        Try to stop server
        """
        if self.isServerExist(serverName):
            cmd = Command(self._STOP_COMMAND + serverName)
            cmd.run(timeout=10, shell=False, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

            c.setDebugMessage('{stopServer} cmd status: ' + str(cmd.status))
            c.setDebugMessage('{stopServer} cmd output: ' + str(cmd.output))
            c.setDebugMessage('{stopServer} cmd error: ' + str(cmd.error))

            if "stopped." in str(cmd.output):
                c.setDebugMessage('{stopServer} Server ' + serverName + ' has been stopped')
                try:
                    query = "UPDATE servers SET status=\'{}\' WHERE serverName=\'{}\';".format('0', serverName)
                    DB(statements=query)
                    c.setInfoMessage(
                        '{stopServer} Server ' + serverName + ' has been updated to status 0')
                except DbException:
                    c.setErrorMessage(
                        '{stopServer} Server ' + serverName + ' cannot be update to status 0')
            else:
                c.setWarningMessage('{stopServer} Server ' + serverName + ' cannot be stopped')

    def createServer(self, serverName):
        """
        Try to create a new server
        """
        if not self.isServerExist(serverName):
            cmd = Command(self._CREATE_COMMAND + serverName)
            cmd.run(timeout=10, shell=False, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

            c.setDebugMessage('{createServer} cmd status: ' + str(cmd.status))
            c.setDebugMessage('{createServer} cmd output: ' + str(cmd.output))
            c.setDebugMessage('{createServer} cmd error: ' + str(cmd.error))

            if "created." in str(cmd.output):
                c.setInfoMessage('{createServer} Server ' + serverName + ' has been created')
                try:
                    query = "INSERT INTO servers values (null, \'{}\', \'{}\', '', '', '', '');".format(serverName, '0')
                    DB(statements=query)
                    c.setInfoMessage(
                        '{createServer} Server ' + serverName + ' has been inserted as a new record to database')
                except DbException:
                    c.setErrorMessage(
                        '{createServer} Server ' + serverName + ' cannot be inserted into database - rollback creation')
                    shutil.rmtree(self._SERVERS_DIR + serverName)

            if "already exists." in str(cmd.output):
                c.setWarningMessage('{createServer} Server ' + serverName + ' has NOT been created - it exists already')

    def isServerExist(self, serverName):
        """
        Check if server folder exist
        true/false
        """
        return os.path.isdir(self._SERVERS_DIR + serverName)

    def getServerStatus(self, serverName):
        """
        Check the status of server
        Also automatically update status of the server anyways
        string from ENUM
        """
        response = None
        status = None

        cmd = Command(self._STATUS_COMMAND + serverName)
        cmd.run(timeout=10, shell=False, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        c.setDebugMessage('{getServerStatus} cmd status: ' + str(cmd.status))
        c.setDebugMessage('{getServerStatus} cmd output: ' + str(cmd.output))
        c.setDebugMessage('{getServerStatus} cmd error: ' + str(cmd.error))

        if "does not exist." in str(cmd.output):
            response = 'DNE'
        if "is not running." in str(cmd.output):
            response = 'INR'
            status = '0'
        if "is running." in str(cmd.output):
            response = 'ISR'
            status = '1'

        if status is not None:
            try:
                query = "UPDATE servers SET status=\'{}\' WHERE serverName=\'{}\';".format(status, serverName)
                DB(statements=query)
                c.setInfoMessage(
                    '{getServerStatus} Server ' + serverName + ' has been updated to status ' + status)
            except DbException:
                c.setErrorMessage(
                    '{getServerStatus} Server ' + serverName + ' cannot be update to status ' + status)

        return response

    def updateServerStatusesOnBoot(self, serverList):
        """
        Check the status of server and update if it is not correct
        Sometime server is restarted and statuses of servers are incorrect in database
        """
        c.setInfoMessage('{updateServerStatusesOnBoot} START')
        for serverName in serverList:
            self.getServerStatus(serverName[0])
        c.setInfoMessage('{updateServerStatusesOnBoot} END')
