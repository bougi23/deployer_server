import datetime
import glob
import os
import re
import shutil
import sys
import zipfile
from os import fdopen, remove
from tempfile import mkstemp

from Models.Configuration import Configuration
from Models.DB import DbException, DB
from Models.wlp.ServerToolsWlp import ServerToolsWlp

c = Configuration()
s = ServerToolsWlp()


# TODO REFACTOR !!!

class DeployWlp(object):
    # Helper variables
    status = False  # Status of deployment True is passed , False is failed
    isWorking = True  # Notification if deployment process is still in progress
    isBoostrapConfigurationUpdatedFlag = False  # Flag if bootstrap.properties has been updated
    isServerXmlConfigurationUpdatedFlag = False  # Flag if server.xml has been updated

    def start(self, serverName):
        """
        Take the serverName and start deployment process
        """
        # Check if server exists
        if not s.isServerExist(serverName):
            c.setDebugMessage('{Deploy} server doesnt exist -> create')
            s.createServer(serverName)
        else:
            # Stop server anyways
            s.stopServer(serverName)

        self._deploymentProccess(serverName)

    def _deploymentProccess(self, serverName):
        """
        Take the serverName and start deployment process
        """
        # 1, Unpack zip to temp folder
        self._unpackZipToTemp(serverName)
        # 2, Move everything from zip file
        self._moveAllFolder(serverName)
        self._copyServerXml(serverName)
        self._copyBootstrapProperties(serverName)
        self._moveRestFiles(serverName)
        # 3, Set when package was deployed
        self._setDeployedDateTime(serverName)
        # 4, Delete unpacked folder when success
        self._deleteDeploymentFolderFromTemp(serverName)
        # 5, Delete zip package as well
        self._deleteDeploymentZipFileFromTemp(serverName)
        # 6, When everything is done leave a trace about it :)
        self.isWorking = False
        self.status = True

    def _moveAllFolder(self, serverName):
        src = c.getTempPath() + serverName + "\\"
        dest = c.getApplicationServerPath() + '\\usr\\servers\\' + serverName + '\\'

        srcFolder = os.listdir(src)
        for folderName in srcFolder:
            fullFolderNameSrc = os.path.join(src, folderName)
            if (os.path.isdir(fullFolderNameSrc)):
                if os.path.exists(os.path.join(dest, folderName)):
                    c.setDebugMessage('{Deploy} Removing already existing folder: ' + folderName)
                    shutil.rmtree(os.path.join(dest, folderName))
                c.setDebugMessage('{Deploy} Copying folder: ' + folderName)
                shutil.move(fullFolderNameSrc, dest)

    def _moveRestFiles(self, serverName):
        src = c.getTempPath() + serverName + "\\"
        dest = c.getApplicationServerPath() + '\\usr\\servers\\' + serverName + '\\'
        excludedFiles = [src + "server.xml", src + "bootstrap.properties"]

        srcFiles = os.listdir(src)
        for fileName in srcFiles:
            fullFileName = os.path.join(src, fileName)
            if (os.path.isfile(fullFileName) and fullFileName not in excludedFiles):
                c.setDebugMessage('{Deploy} Copying file: ' + str(fileName))
                shutil.copy(fullFileName, dest)

    def getStatus(self):
        """
        Returns True when particular deployment is done or False
        """
        return self.status

    def getIsWorking(self):
        """
        Returns True when deployment process is still wworkingor False
        """
        return self.isWorking

    def getIsBootstrapConfigurationUpdated(self):
        """
        Returns True when bootstrap.properties configuration has been successfully updated or False when NOT
        """
        return self.isBoostrapConfigurationUpdatedFlag

    def getIsServerXmlConfigurationUpdated(self):
        """
        Returns True when server.xml configuration has been successfully updated or False when NOT
        """
        return self.isServerXmlConfigurationUpdatedFlag

    def getIsConfigurationUpdated(self):
        """
        Returns True when server.xml and bootstrap.properties configuration were successfully updated or False when NOT
        """
        return self.isBoostrapConfigurationUpdatedFlag and self.isServerXmlConfigurationUpdatedFlag

    def _unpackZipToTemp(self, serverName):
        """
        Take the zipfile and Unpack to temp dir
        """
        try:
            c.setInfoMessage('{Deploy} Unpacking ZIP file to TEMP folder')
            self._deleteDeploymentFolderFromTemp(serverName)
            zip_ref = zipfile.ZipFile(c.getTempPath() + serverName + '.zip', 'r')
            zip_ref.extractall(c.getTempPath() + serverName)
            zip_ref.close()
        except:
            c.setErrorMessage('{Deploy} Something went wrong in unpackZipToTemp method')
            self.isWorking = False
            self.status = False

    def _checkDeploymentStructure(self, serverName):
        """
        Check if deployment is according to schema
        """
        tempDeploymentDir = c.getTempPath() + serverName + '\\'

        if self._isDeploymentSchemaCompliant(tempDeploymentDir):
            self._copyDeployment(serverName)
        else:
            c.setErrorMessage('{Deploy} Something went wrong in checkDeploymentStructure method')
            # Delete tempDeploymentDir
            self._deleteDeploymentFolderFromTemp(serverName)
            self.isWorking = False
            self.status = False

    def _isDeploymentSchemaCompliant(self, tempDeploymentDir):
        """
        Will check structure of just extracted deployment package for MANDATORY minimal schema structure
        Checks for apps folder, server.xml file and bootstrap.properties file
        """
        c.setInfoMessage('{Deploy} Checking deployment package structure')

        result = False
        if (os.path.isdir(tempDeploymentDir + 'apps') and os.path.exists(
                tempDeploymentDir + 'server.xml') and os.path.exists(
            tempDeploymentDir + 'bootstrap.properties')):
            c.setInfoMessage('{Deploy} Deployment package is compliant with minimal schema needs')
            result = True
        else:
            c.setErrorMessage('{Deploy} Package is not compliant with minimal schema needs')

        return result

    def _copyWarFile(self, serverName):
        """
        Take the serverName and starts copying war file(s) to destination folder
        """
        src = c.getTempPath() + serverName + '\\apps\\'
        dest = c.getApplicationServerPath() + '\\usr\\servers\\' + serverName + '\\apps\\'
        for filename in glob.glob(os.path.join(src, '*.*')):
            shutil.copy(filename, dest)

    def _copyServerXml(self, serverName):
        """
        Take the serverName and starts copying server.xml file to destination folder
        """
        src = c.getTempPath() + serverName + '\\server.xml'
        dest = c.getApplicationServerPath() + '\\usr\\servers\\' + serverName + '\\server.xml'

        serverContextValue = None

        if os.path.exists(src):
            try:
                regexp = re.compile(r'context-root="(.*?)"')
                with open(src) as file:
                    for line in file:
                        match = regexp.search(line)
                        if match:
                            serverContextValue = match.group(1)

                shutil.copyfile(src, dest)
            except IOError as e:
                print("Unable to copy file. %s" % e)
            except:
                print("Unexpected error:", sys.exc_info())

            if serverContextValue is not None:
                try:
                    query = "UPDATE servers SET contextRoot=\'{}\'  WHERE serverName=\'{}\';".format(serverContextValue,
                                                                                                     serverName)
                    DB(statements=query)
                    c.setInfoMessage(
                        '{Deploy} ContextRoot has been updated in database for ' + serverName)
                except DbException:
                    c.setErrorMessage(
                        '{Deploy} ContextRoot cannot be updated in database for ' + serverName)
        else:
            c.setErrorMessage('{Deploy} server.xml is not in the package')

    def _copyBootstrapProperties(self, serverName):
        """
        Take the serverName and starts copying bootstrap.properties to destination folder
        """
        src = c.getTempPath() + serverName + '\\bootstrap.properties'
        dest = c.getApplicationServerPath() + '\\usr\\servers\\' + serverName + '\\bootstrap.properties'

        portValues = {}

        if os.path.exists(src):
            try:
                with open(src) as file:
                    for line in file:
                        name, var = line.partition("=")[::2]
                        portValues[name.strip()] = var

                shutil.copyfile(src, dest)
            except IOError as e:
                print("Unable to copy file. %s" % e)
            except:
                print("Unexpected error:", sys.exc_info())

            http = portValues["default.http.port"].strip().rstrip('\n')
            https = portValues["default.https.port"].strip().rstrip('\n')

            try:
                query = "UPDATE servers SET http=\'{}\', https=\'{}\'  WHERE serverName=\'{}\';".format(http, https,
                                                                                                        serverName)
                DB(statements=query)
                c.setInfoMessage(
                    '{Deploy} Ports has been updated in database for ' + serverName)
            except DbException:
                c.setErrorMessage(
                    '{Deploy} Ports cannot be updated in database for ' + serverName)
        else:
            c.setErrorMessage('{Deploy} bootstrap.properties is not in the package')

    def _deleteDeploymentZipFileFromTemp(self, serverName):
        """
        Delete deployment zip file from Temp
        """
        try:
            # Remove file
            tempDeploymentZipFile = c.getTempPath() + serverName + '.zip'
            if os.path.exists(str(tempDeploymentZipFile)):
                remove(tempDeploymentZipFile)
        except IOError:
            print("{Deploy} Something went wrong in _deleteDeploymentZipFileFromTemp method / IO exception")

    def _setDeployedDateTime(self, serverName):
        """
        Set current dateTime for deployedDateTime
        """
        now = datetime.datetime.now()
        deployedDatetime = now.strftime("%Y-%m-%d %H:%M")
        try:
            query = "UPDATE servers SET deployedDateTime=\'{}\'  WHERE serverName=\'{}\';".format(deployedDatetime,
                                                                                                  serverName)
            DB(statements=query)
            c.setInfoMessage(
                '{Deploy} deployerDateTime has been updated in database for ' + serverName)
        except DbException:
            c.setErrorMessage(
                '{Deploy} deployerDateTime cannot be updated in database for ' + serverName)

    def _deleteDeploymentFolderFromTemp(self, serverName):
        """
        Delete deployment temporary folder
        """
        try:
            # Remove folder (if exists) with all files
            tempDeploymentDir = c.getTempPath() + serverName
            if os.path.isdir(str(tempDeploymentDir)):
                shutil.rmtree(tempDeploymentDir, ignore_errors=True)
        except IOError:
            print("{Deploy} Something went wrong in _deleteDeploymentFolderFromTemp method / IO exception")

    def _updateBootstrapConfiguration(self, serverName, http, https):
        """
        Update http and https ports in bootstrap.properties files
        Takes : serverName, http, https
        """
        boostrapDest = c.getApplicationServerPath() + '\\usr\\servers\\' + serverName + '\\bootstrap.properties'

        if serverName is not None:
            if s.getServerStatus(serverName) == 'ISR':
                s.stopServer(serverName)

        # Modify boostrap properties
        if os.path.exists(boostrapDest):

            patternHttp = 'default.http.port\s*=\s*([^\s]+)'
            substHttp = 'default.http.port=' + http
            self._replacePatternInFile(boostrapDest, patternHttp, substHttp)

            patternHttps = 'default.https.port\s*=\s*([^\s]+)'
            substHttps = 'default.https.port=' + https
            self._replacePatternInFile(boostrapDest, patternHttps, substHttps)

            try:
                query = "UPDATE servers SET http=\'{}\', https=\'{}\'  WHERE serverName=\'{}\';".format(http, https,
                                                                                                        serverName)
                DB(statements=query)
                c.setInfoMessage(
                    '{Deploy} Configuration (ports) has been updated in database for ' + serverName)
                self.isBoostrapConfigurationUpdatedFlag = True
            except DbException:
                c.setErrorMessage(
                    '{Deploy} Configuration (ports) cannot be updated in database for ' + serverName)

    def _updateServerXmlConfiguration(self, serverName, contextRoot):
        """
        Update context-root in server.xml
        Takes : serverName, contextRoot
        """
        serverXmlDest = c.getApplicationServerPath() + '\\usr\\servers\\' + serverName + '\\server.xml'

        if serverName is not None:
            if s.getServerStatus(serverName) == 'ISR':
                s.stopServer(serverName)

        # Modify server.xml properties
        if os.path.exists(serverXmlDest):
            pattern = 'context-root=\"(.*?)\"'
            subst = 'context-root=\"' + contextRoot + '\"'
            self._replacePatternInFile(serverXmlDest, pattern, subst)

            try:
                query = "UPDATE servers SET contextRoot=\'{}\'  WHERE serverName=\'{}\';".format(contextRoot,
                                                                                                 serverName)
                DB(statements=query)
                c.setInfoMessage(
                    '{Deploy} Configuration (contextRoot) has been updated in database for ' + serverName)
                self.isServerXmlConfigurationUpdatedFlag = True
            except DbException:
                c.setErrorMessage(
                    '{Deploy} Configuration (contextRoot) cannot be updated in database for ' + serverName)

    def updateConfiguration(self, serverName, contextRoot, http, https):
        """
        Update existing configuration
        Takes : serverName, contextRoot, http , http
        """
        # here is regex to replace contextroot in server.xml -> context-root=\"(.*?)\"
        # here is regex to replace key value in bootstrap.xml -> default.http.port=([^\s]+)

        # Update bootstrap first
        self._updateBootstrapConfiguration(serverName, http, https)

        # Update server.xml
        self._updateServerXmlConfiguration(serverName, contextRoot)

    def _replacePatternInFile(self, filePath, pattern, subst):
        """
        Replace found rexec in file and save it
        Takes : filePath, pattern, subst
        """
        # Create temp file
        fh, abs_path = mkstemp()
        with fdopen(fh, 'w') as new_file:
            with open(filePath) as old_file:
                for line in old_file:
                    new_file.write(re.sub(pattern, subst, line))
        # Remove original file
        remove(filePath)
        # Move new file
        shutil.move(abs_path, filePath)
