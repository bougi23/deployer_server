from flask import Flask, render_template, logging, request
from flask_restful import Api

from Models.Configuration import Configuration
from Models.DB import DB, DbException
from Models.Resources.get.ServerList import ServerList
from Models.Resources.get.ServerLogs import ServerLogs
from Models.Resources.get.ServerStatus import ServerStatus
from Models.Resources.get.TestPage import TestPage
from Models.Resources.post.AddUserAuthUser import AddUser, AuthenticateUser
from Models.Resources.post.ConfigureServer import ConfigureServer
from Models.Resources.post.CreateServer import CreateServer
from Models.Resources.post.DeleteServer import DeleteServer
from Models.Resources.post.LockUnlock import Lock, Unlock
from Models.Resources.post.StartServer import StartServer
from Models.Resources.post.StopServer import StopServer
from Models.Resources.post.UploadDeployment import UploadDeployment
from Models.wlp.ServerToolsWlp import ServerToolsWlp


class Deployer(object):
    _c = Configuration()
    _app = None
    _version_ = '0.2rc'

    def __init__(self):
        # Init app
        self._c.setInfoMessage('{Deployer} Starting Deployer ....')
        #
        self._app = Flask(__name__, template_folder='Resources\\templates',
                          static_folder='Resources\\templates\\static')

        # Init rest API and web GUI
        self._initialize()
        # Check and propagate server statuses on boot
        if 'wlp' in self._c.getMode():
            self._checkServerStatusesWLP()

        if 'wildfly' in self._c.getMode():
            pass

        self._c.setInfoMessage('{Deployer} Deployer has been STARTED')

    def _checkServerStatusesWLP(self):
        s = ServerToolsWlp()
        serverList = []
        try:
            query = "select serverName from servers;"
            db = DB(statements=query)
            serverList = db.getSqlDataResult()
        except DbException:
            self._c.setErrorMessage('{Deployer} Cannot select data from table servers')

        s.updateServerStatusesOnBoot(serverList)

    def _initialize(self):
        api = Api(self._app)

        # Disable FLASK logger when debug is False in configuration
        if 'False' in self._c.getDebug():
            self._app.logger.disabled = True
            log = logging.getLogger('werkzeug')
            log.disabled = True

        # Init of all GETs
        api.add_resource(TestPage, '/api/')  # Test Page
        api.add_resource(ServerStatus, '/api/serverStatus/<serverName>')  # ServerStatus
        api.add_resource(ServerList, '/api/serverList')  # ServerList
        api.add_resource(ServerLogs, '/api/serverLogs/<serverName>')  # ServerStatus

        # Init of all POSTs
        api.add_resource(CreateServer, '/api/createServer/<serverName>')  # CreateServer
        api.add_resource(StartServer, '/api/startServer/<serverName>')  # StartServer
        api.add_resource(StopServer, '/api/stopServer/<serverName>')  # StoptServer
        api.add_resource(UploadDeployment, '/api/uploadDeployment/<serverName>')  # UploadDeployment
        api.add_resource(DeleteServer, '/api/deleteServer/<serverName>')  # DeleteServer
        api.add_resource(ConfigureServer, '/api/configureServer/<serverName>')  # ConfigureServer

        # Lock/unlock prototype
        api.add_resource(Lock, '/api/lock/<uid>')  # lock
        api.add_resource(Unlock, '/api/unlock')  # unlock

        # addUser
        api.add_resource(AddUser, '/api/addUser')  # add user
        # authUser
        api.add_resource(AuthenticateUser, '/api/authUser')  # authenticate user

        @self._app.route('/')
        def index():
            auth = request.cookies.get('authenticated')
            response = None
            if auth is not None and 'True' in auth:
                response = management()
            else:
                response = login()

            return response

        # Init of Management page
        def management():
            self._c.setDebugMessage('{Management} Management page has been requested to render')

            serverList = []
            try:
                query = "select serverName, status, http, https, contextRoot, deployedDateTime from servers;"
                db = DB(statements=query)
                serverList = db.getSqlDataResult()
            except DbException:
                self._c.setErrorMessage('{Deployer} Cannot select data from table servers')

            # render management template
            return render_template('management.html', addr=self._c.getServerIpAddress(),
                                   port=self._c.getServerPort(), environment=self._c.getEnvironmentName(),
                                   servers=serverList, version=self._version_)

        # Init login page
        def login():
            self._c.setDebugMessage('{Login} Login page has been requested to render')
            return render_template('login.html', authentication=self._c.getAuthentication(),
                                   addr=self._c.getServerIpAddress(),
                                   port=self._c.getServerPort(), version=self._version_)

    def getApp(self):
        return self._app
