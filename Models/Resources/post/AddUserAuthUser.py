import hashlib

from flask import request
from flask_jsonpify import jsonpify
from flask_restful import Resource

from Models.Configuration import Configuration
from Models.DB import DB, DbException

c = Configuration()


class AddUser(Resource):
    def post(self):
        c.setDebugMessage('API POST call addUser has been received')
        data = {}
        content = request.get_json(silent=True)
        data['status'] = '0'

        if content['userName'] is not None and content['password'] is not None:
            userName = content['userName']
            password = content['password']
            if (self.addUserToDatabase(userName, password)):
                data['status'] = '1'

        return jsonpify(data)

    def addUserToDatabase(self, userName, password):
        """
        username
        password in plain
        return: boolean
        """
        response = False
        passwordHashed = hashlib.md5(password.encode('utf-8')).hexdigest()

        try:
            query = "INSERT INTO users values (null, \'{}\', \'{}\');".format(userName, passwordHashed)
            DB(statements=query)
            c.setInfoMessage('{AddUserAuthUser} User ' + userName + ' has been added')
            response = True
        except DbException:
            c.setErrorMessage('{AddUserAuthUser} User ' + userName + ' has NOT been created')

        return response


class AuthenticateUser(Resource):
    def post(self):
        c.setDebugMessage('API POST call authenticateUser has been received')
        data = {}
        content = request.get_json(silent=True)
        if 'True' in c.getAuthentication():
            if content['userName'] is not None and content['password'] is not None:
                userName = content['userName']
                password = content['password']
                if (self.authenticate(userName, password)):
                    data['status'] = '1'
                else:
                    data['status'] = '0'
            else:
                data['status'] = '0'
        else:
            if content['userName'] is not None:
                data['status'] = '1'
            else:
                data['status'] = '0'

        return jsonpify(data)

    def authenticate(self, userName, password):
        """
        username
        password in MD5
        return: boolean
        """
        response = False
        adminPasswordHashed = hashlib.md5(c.getAdminPassword().encode('utf-8')).hexdigest()

        if 'administrator' in userName.lower() and adminPasswordHashed in password:
            response = True
        else:
            dbPassword = []
            try:
                query = "SELECT password FROM users WHERE userName=\'{}\';".format(userName)
                db = DB(statements=query)
                dbPassword = db.getSqlDataResult()
            except DbException:
                c.setErrorMessage('{AddUserAuthUser} Cannot select data from table user')

            if dbPassword and password in dbPassword[0]:
                response = True

        return response
