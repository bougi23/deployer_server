from flask import request
from flask_jsonpify import jsonpify
from flask_restful import Resource

from Models.Configuration import Configuration
from Models.wlp.DeployWlp import DeployWlp
from Models.wlp.ServerToolsWlp import ServerToolsWlp

c = Configuration()
s = ServerToolsWlp()


class ConfigureServer(Resource):
    def post(self, serverName):
        c.setInfoMessage('{configureServer} API POST call configureServer has been received')
        if 'wlp' in c.getMode():
            return self._configureWLP(serverName)

        if 'wildfly' in c.getMode():
            return self._configureWILDFLY(serverName)

    def _configureWLP(self, serverName):
        s = ServerToolsWlp()
        data = {}

        if serverName is not None:
            if s.isServerExist(serverName):
                content = request.get_json(silent=True)
                if content is not None:
                    if self._isAllElementsPresent(content):
                        c.setDebugMessage('{configureServer} Sent json data are compliant and contains all elements')
                        #############
                        # Call configuration upgrade here -> Introduce method in DeployWlp.py
                        #############
                        d = DeployWlp()
                        d.updateConfiguration(serverName, content['contextRoot'], content['http'], content['https'])
                        if d.getIsConfigurationUpdated():
                            data['status'] = '1'
                        else:
                            c.setDebugMessage(
                                '{configureServer} Update failed in updateBootstrapConfiguration method')
                            data['status'] = '0'
                    else:
                        c.setDebugMessage('{configureServer} Sent json data are NOT compliant')
                        data['status'] = '0'
                else:
                    c.setDebugMessage('{configureServer} Sent json data are missing completely')
                    data['status'] = '0'
            else:
                c.setDebugMessage('{configureServer} Server doesnt exist')
                data['status'] = '0'

        return jsonpify(data)

    def _configureWILDFLY(self, serverName):
        data = {}
        # not yet implemented
        data['status'] = '0'
        data['output'] = 'WILDFLY not implemented yet ....'

        return jsonpify(data)

    def _isAllElementsPresent(self, content):
        contextRootElement = True if "contextRoot" in content else False
        httpElement = True if "http" in content else False
        httpsElement = True if "https" in content else False

        return contextRootElement and httpElement and httpsElement
