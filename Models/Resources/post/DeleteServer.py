from flask_jsonpify import jsonpify
from flask_restful import Resource

from Models.Configuration import Configuration
from Models.wlp.ServerToolsWlp import ServerToolsWlp

c = Configuration()


class DeleteServer(Resource):
    def post(self, serverName):
        c.setInfoMessage('{deleteServer} API POST call deleteServer has been received')
        if 'wlp' in c.getMode():
            return self._deleteWLP(serverName)

        if 'wildfly' in c.getMode():
            return self._deleteWILDFLY(serverName)

    def _deleteWLP(self, serverName):
        s = ServerToolsWlp()
        data = {}

        if serverName is not None:
            s.deleteServer(serverName)
            if not s.isServerExist(serverName):
                data['status'] = '1'
            else:
                data['status'] = '0'
        else:
            data['status'] = '0'

        return jsonpify(data)

    def _deleteWILDFLY(self, serverName):
        data = {}
        # not yet implemented
        data['status'] = '0'
        data['output'] = 'WILDFLY not implemented yet ....'

        return jsonpify(data)
