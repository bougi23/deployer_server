from flask_jsonpify import jsonpify
from flask_restful import Resource

from Models.Configuration import Configuration
from Models.wlp.ServerToolsWlp import ServerToolsWlp

c = Configuration()


class StartServer(Resource):
    def post(self, serverName):
        c.setInfoMessage('{startServer} API POST call startServer has been received')
        if 'wlp' in c.getMode():
            return self._startWLP(serverName)

        if 'wildfly' in c.getMode():
            return self._startWILDFLY(serverName)

    def _startWLP(self, serverName):
        s = ServerToolsWlp()
        data = {}

        if serverName is not None:
            c.setDebugMessage('{startServer} Old server status ' + s.getServerStatus(serverName))
            s.startServer(serverName)
            if 'ISR' in s.getServerStatus(serverName):
                data['status'] = '1'
            else:
                data['status'] = '0'
        else:
            data['status'] = '0'

        c.setDebugMessage('{startServer} New server status ' + s.getServerStatus(serverName))
        return jsonpify(data)

    def _startWILDFLY(self, serverName):
        data = {}
        # not yet implemented
        data['status'] = '0'
        data['output'] = 'WILDFLY not implemented yet ....'

        return jsonpify(data)
