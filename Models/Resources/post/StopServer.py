from flask_jsonpify import jsonpify
from flask_restful import Resource

from Models.Configuration import Configuration
from Models.wlp.ServerToolsWlp import ServerToolsWlp

c = Configuration()


class StopServer(Resource):
    def post(self, serverName):
        c.setInfoMessage('{stopServer} API POST call stopServer has been received')
        if 'wlp' in c.getMode():
            return self._stopWLP(serverName)

        if 'wildfly' in c.getMode():
            return self._stopWILDFLY(serverName)

    def _stopWLP(self, serverName):
        s = ServerToolsWlp()
        data = {}

        if serverName is not None:
            c.setDebugMessage('{stopServer} Old server status ' + s.getServerStatus(serverName))
            s.stopServer(serverName)
            if 'INR' in s.getServerStatus(serverName):
                data['status'] = '1'
            else:
                data['status'] = '0'
        else:
            data['status'] = '0'

        c.setDebugMessage('{stopServer} New server status ' + s.getServerStatus(serverName))
        return jsonpify(data)

    def _stopWILDFLY(self, serverName):
        data = {}
        # not yet implemented
        data['status'] = '0'
        data['output'] = 'WILDFLY not implemented yet ....'

        return jsonpify(data)
