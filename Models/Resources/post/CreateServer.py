from flask_jsonpify import jsonpify
from flask_restful import Resource

from Models.Configuration import Configuration
from Models.wlp.ServerToolsWlp import ServerToolsWlp

c = Configuration()


class CreateServer(Resource):
    def post(self, serverName):
        c.setInfoMessage('{createServer} API POST call createServer has been received')
        if 'wlp' in c.getMode():
            return self._createWLP(serverName)

        if 'wildfly' in c.getMode():
            return self._createWILDFLY(serverName)

    def _createWLP(self, serverName):
        s = ServerToolsWlp()
        data = {}

        if serverName is not None:
            s.createServer(serverName)
            if s.isServerExist(serverName):
                data['status'] = '1'
            else:
                data['status'] = '0'

        return jsonpify(data)

    def _createWILDFLY(self, serverName):
        data = {}
        # not yet implemented
        data['status'] = '0'
        data['output'] = 'WILDFLY not implemented yet ....'

        return jsonpify(data)
