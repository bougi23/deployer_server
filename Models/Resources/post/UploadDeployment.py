import os
import time

from flask import request
from flask_jsonpify import jsonpify
from flask_restful import Resource

from Models.Configuration import Configuration
from Models.wlp.DeployWlp import DeployWlp

c = Configuration()

ALLOWED_EXTENSIONS = set(['zip'])


class UploadDeployment(Resource):
    def post(self, serverName):
        c.setInfoMessage('{stopServer} API POST call uploadDeployment has been received')
        if 'wlp' in c.getMode():
            return self._uploadDeploymentWLP(serverName)

        if 'wildfly' in c.getMode():
            return self._uploadDeploymentWILDFLY(serverName)

    def _allowedFileExtensions(self, filename):
        return '.' in filename and \
               filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

    def _uploadDeploymentWLP(self, serverName):
        data = {}

        if serverName is not None:

            file = request.files['file']
            newFileName = serverName + '.zip'

            if file and self._allowedFileExtensions(file.filename):
                # first delete file with same name if any
                try:
                    os.remove(c.getTempPath() + newFileName)
                except OSError:
                    pass

                # Check if folder exists before save
                if not os.path.exists(c.getTempPath()):
                    os.makedirs(c.getTempPath())

                file.save(os.path.join(c.getTempPath(), newFileName))

                # Deploy on WLP now
                deploy = DeployWlp()
                deploy.start(serverName)
                # and follow the progress of deployment
                while deploy.isWorking:
                    c.setDebugMessage('{UploadDeployment} Deployment process is working in background ...')
                    time.sleep(1)
                if not deploy.isWorking and deploy.status:
                    data['status'] = '1'
                else:
                    data['status'] = '0'

            else:
                data['output'] = 'Missing file as data or wrong extension - allowed is zip only'
                data['status'] = '0'

        return jsonpify(data)

    def _uploadDeploymentWILDFLY(self, serverName):
        data = {}
        # not yet implemented
        data['status'] = '0'
        data['output'] = 'WILDFLY not implemented yet ....'

        return jsonpify(data)
