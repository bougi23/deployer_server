from flask_jsonpify import jsonpify
from flask_restful import Resource

from Models.Configuration import Configuration

c = Configuration()


class Lock(Resource):
    def post(self, uid):
        c.setDebugMessage('API POST call lock has been received with lockID: ' + uid)
        data = {}
        if c.getLockUid() is None:
            c.setDebugMessage('Lock created')
            c.setLockUid(uid)
            data['uid'] = c.getLockUid()
            data['status'] = '1'
        else:
            c.setDebugMessage('Its already locked by: ' + c.getLockUid())
            data['uid'] = c.getLockUid()
            data['status'] = '0'

        return jsonpify(data)


class Unlock(Resource):
    def post(self):
        c.setDebugMessage('API POST call unlock has been received')
        data = {}
        if c.getLockUid() is not None:
            c.setLockUid(None)
        data['status'] = '1'

        return jsonpify(data)
