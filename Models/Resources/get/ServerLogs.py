import calendar
import os
import shutil
import time

from flask import send_file
from flask_jsonpify import jsonpify
from flask_restful import Resource

from Models.Configuration import Configuration

c = Configuration()


class ServerLogs(Resource):

    def get(self, serverName):
        c.setInfoMessage('{ServerLogs} API GET call ServerLogs has been received')
        if 'wlp' in c.getMode():
            return self._logsWLP(serverName)

        if 'wildfly' in c.getMode():
            return self._logsWILDFLY(serverName)

    def _logsWLP(self, serverName):
        data = {}
        data['status'] = '0'
        path = c.getApplicationServerPath() + "usr/servers/" + serverName + "/logs/"

        now = calendar.timegm(time.gmtime())

        zipFileNameWoutExtension = c.getTempPath() + serverName + '_logs_' + str(now)
        zipFileNameWExtension = zipFileNameWoutExtension + '.zip'

        if serverName is not None:
            try:
                shutil.make_archive(zipFileNameWoutExtension, 'zip', path)
                return send_file(os.path.abspath(zipFileNameWExtension), as_attachment=True)
            except Exception:
                c.setErrorMessage('{ServerLogs} Something went wrong during zipping log files.')

        return jsonpify(data)

    def _logsWILDFLY(self, serverName):
        data = {}
        # not yet implemented
        data['status'] = '0'
        data['output'] = 'WILDFLY not implemented yet ....'

        return jsonpify(data)
