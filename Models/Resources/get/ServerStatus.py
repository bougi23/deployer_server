from flask_jsonpify import jsonpify
from flask_restful import Resource

from Models.Configuration import Configuration
from Models.wlp.ServerToolsWlp import ServerToolsWlp

c = Configuration()


class ServerStatus(Resource):
    def get(self, serverName):
        c.setInfoMessage('{serverStatus} API GET call serverStatus has been received')
        if 'wlp' in c.getMode():
            return self._statusWLP(serverName)

        if 'wildfly' in c.getMode():
            return self._statusWILDFLY(serverName)

    def _statusWLP(self, serverName):
        s = ServerToolsWlp()
        data = {}

        if serverName is not None:
            data['status'] = '1'
            data['output'] = s.getServerStatus(serverName)
        else:
            data['status'] = '0'

        return jsonpify(data)

    def _statusWILDFLY(self, serverName):
        data = {}
        # not yet implemented
        data['status'] = '0'
        data['output'] = 'WILDFLY not implemented yet ....'

        return jsonpify(data)
