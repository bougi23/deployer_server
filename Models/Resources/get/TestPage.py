from flask_jsonpify import jsonpify
from flask_restful import Resource


class TestPage(Resource):
    def get(self):
        data = {}
        data['status'] = '1'

        return jsonpify(data)
