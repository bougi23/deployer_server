from flask_jsonpify import jsonpify
from flask_restful import Resource

from Models.Configuration import Configuration
from Models.DB import DB, DbException

c = Configuration()


class ServerList(Resource):
    def get(self):
        c.setInfoMessage('{serverList} API GET call serverList has been received')
        if 'wlp' in c.getMode():
            return self._serverListWLP()

        if 'wildfly' in c.getMode():
            return self._serverListWILDFLY()

    def _serverListWLP(self):
        data = {}

        serversQuery = self._getServerList()
        if serversQuery is not None:

            listOfServers = []
            for serverItem in serversQuery:
                server = {}
                server['serverName'] = serverItem[0]
                server['status'] = serverItem[1]
                server['http'] = serverItem[2]
                server['https'] = serverItem[3]
                server['contextRoot'] = serverItem[4]
                server['deployedDateTime'] = serverItem[5]
                # append server to listOfServers
                listOfServers.append(server)
            # return listOfServers
            data['servers'] = listOfServers
            data['status'] = '1'
        else:
            data['status'] = '0'

        return jsonpify(data)

    def _serverListWILDFLY(self):
        data = {}
        # not yet implemented
        data['status'] = '0'
        data['output'] = 'WILDFLY not implemented yet ....'

        return jsonpify(data)

    def _getServerList(self):
        serverList = None
        try:
            query = "select serverName, status, http, https, contextRoot, deployedDateTime from servers;"
            db = DB(statements=query)
            serverList = db.getSqlDataResult()
        except DbException:
            c.setErrorMessage('{Deployer} Cannot select data from table servers')

        return serverList
