    /////////////////////////////////////////////////////////
    // Start Server
    /////////////////////////////////////////////////////////
    function startServer(addr, port ,serverName) {
        // disable all buttons
        $(':button').prop('disabled', true)
        // show loading dialog
        showLoadingDialog();
        //
        _callStartServer(addr, port, serverName);
    }

    function _callStartServer(addr, port, serverName){
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                var jsonResponse = JSON.parse(xhr.responseText);
                if(jsonResponse.status == '1') {
                    location.reload(true);
                }
                else{
                    alert('Something went wrong as per response from the server is : ' + xhr.responseText);
                    location.reload(true);
                }
            }
        }
        xhr.open('POST', 'http://' +  addr + ':' + port + '/api/startServer/' + serverName, true);
        xhr.send(null);
     }

    /////////////////////////////////////////////////////////
    // Add User
    /////////////////////////////////////////////////////////
    function showAddUserDialog(addr, port, serverName){
        $("#addUserDialog").modal("show");

        // Save button actions
        $('.addUserFormSaver').on('click', function () {
            $("#addUserDialog").modal('hide');
            // disable all buttons
            $(':button').prop('disabled', true)
            // show loading dialog
            showLoadingDialog();
            // construct json here and pass to send
            var $items = $('#userName, #password')
            var payload = {}
            $items.each(function() {
                payload[this.id] = $(this).val()
            })
            //
            _callAddUser(addr, port, payload)
        });
    }

    function _callAddUser(addr, port, payload){
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
                if (xhr.readyState == XMLHttpRequest.DONE) {
                var jsonResponse = JSON.parse(xhr.responseText);
                if(jsonResponse.status == '1') {
                    location.reload(true);
                }
                else{
                    alert('Something went wrong as per response from the server is : ' + xhr.responseText);
                    location.reload(true);
                }
            }
        }
        xhr.open('POST', 'http://' +  addr + ':' + port + '/api/addUser', true);
        xhr.setRequestHeader("Content-Type", "application/json");

        xhr.send(JSON.stringify(payload));

    }


    /////////////////////////////////////////////////////////
    // Upload Deployment
    /////////////////////////////////////////////////////////
    function showUploadDeploymentDialog(addr, port, serverName){
        $("#uploadDialog").modal("show");

        // Save button actions
        $('.uploadFormSaver').on('click', function () {
            // TODO check if file is choosen !!!
            $("#uploadDialog").modal('hide');
            // disable all buttons
            $(':button').prop('disabled', true)
            // show loading dialog
            showLoadingDialog();
            //
            _callUploadDeploymentServer(addr, port, serverName)
        });
    }

    function _callUploadDeploymentServer(addr, port, serverName){
        var formData = new FormData();
        formData.append("file", document.getElementById("deploymentFile").files[0]);

        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                var jsonResponse = JSON.parse(xhr.responseText);
                if(jsonResponse.status == '1') {
                    location.reload(true);
                }
                else{
                    alert('Something went wrong as per response from the server is : ' + xhr.responseText);
                    location.reload(true);
                }
            }
        }
        xhr.open('POST', 'http://' +  addr + ':' + port + '/api/uploadDeployment/' + serverName, true);
        xhr.send(formData);
    }

    /////////////////////////////////////////////////////////
    // Stop Server
    /////////////////////////////////////////////////////////
    function stopServer(addr, port ,serverName) {
        // disable all buttons
        $(':button').prop('disabled', true)
        // show loading dialog
        showLoadingDialog();
        //
        _callStopServer(addr, port, serverName);
    }

    function _callStopServer(addr, port, serverName){
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                var jsonResponse = JSON.parse(xhr.responseText);
                if(jsonResponse.status == '1') {
                    location.reload(true);
                }
                else{
                    alert('Something went wrong as per response from the server is : ' + xhr.responseText);
                    location.reload(true);
                }
            }
        }
        xhr.open('POST', 'http://' +  addr + ':' + port + '/api/stopServer/' + serverName, true);
        xhr.send(null);
    }

    /////////////////////////////////////////////////////////
    // Remove Server
    /////////////////////////////////////////////////////////
    function showRemoveServerDialog(addr, port, serverName){
        // Show Modal
        $('#removeServerDialog').modal('show');
        // Set text
        $("#removeServerText").text("Do you really want to remove server '" + serverName + "' completely?");
        // Perform action on positive button
        $('.removeServerSaver').on('click', function () {
            // disable all buttons
            $(':button').prop('disabled', true)
            $("#removeServerDialog").modal('hide');
            // show loading dialog
            showLoadingDialog();
            //
            _callRemoveServer(addr, port, serverName)
        });

        // Close button actions'
        $('.removeServerCloser').on('click', function () {
            $("#removeServerDialog").dialog('close');
        });
    }

    function _callRemoveServer(addr, port ,serverName) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                var jsonResponse = JSON.parse(xhr.responseText);
                if(jsonResponse.status == '1') {
                    location.reload(true);
                }
                else{
                    alert('Something went wrong as per response from the server is : ' + xhr.responseText);
                    location.reload(true);
                }
            }
        }
        xhr.open('POST', 'http://' +  addr + ':' + port + '/api/deleteServer/' + serverName, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(null);
    }

    /////////////////////////////////////////////////////////
    // Configure Server
    /////////////////////////////////////////////////////////
    function showConfigurationDialog(addr, port, contextRoot, http, https, serverName){
        $("#configurationDialog").modal("show");
        // set the input boxes values if any
        $('.contextRoot').val(contextRoot)
        $('.http').val(http)
        $('.https').val(https)


        // Save button actions
        $('.configurationFormSaver').on('click', function () {

            isContextRoot = false;
            var contextRootValue = $("#contextRoot").val();

            isHttp = false;
            var httpValue = $("#http").val();

            isHttps = false;
            var httpsValue = $("#https").val();

            var specials=/[^a-zA-Z0-9]/;

            if(contextRootValue !== null && contextRootValue !== '' && !specials.test(contextRootValue)) {
                isContextRoot = true;
                document.getElementById("contextRoot").style.borderColor = "initial";
            }
            else{
                isContextRoot = false;
                document.getElementById('contextRoot').style.borderColor='#F51313';
            }

            var specials=/[^0-9]/;
            if(httpValue !== null && httpValue !== '' && !specials.test(httpValue)) {
                isHttp = true;
                document.getElementById("http").style.borderColor = "initial";
            }
            else{
                isHttp = false;
                document.getElementById('http').style.borderColor='#F51313';
            }

            if(httpsValue !== null && httpsValue !== '' && !specials.test(httpsValue)) {
                isHttps = true;
                document.getElementById("https").style.borderColor = "initial";
            }
            else{
                isHttps = false;
                document.getElementById('https').style.borderColor='#F51313';
            }

            if(isContextRoot && isHttp && isHttps){
                $("#configurationDialog").modal('hide');
                showLoadingDialog()
                // construct json here and pass to send
                var $items = $('#contextRoot, #http, #https ')
                var payload = {}
                $items.each(function() {
                    payload[this.id] = $(this).val()
                })
                //
                _callConfigurationUpdate(addr, port, serverName, payload)
            }
        });

        // Close button actions
            $('.configurationFormCloser').on('click', function () {
                document.getElementById("contextRoot").style.borderColor = "initial";
                document.getElementById("http").style.borderColor = "initial";
                document.getElementById("https").style.borderColor = "initial";
            $("#configurationDialog").dialog('close');
        });
    }

    function _callConfigurationUpdate(addr, port, serverName, payload){
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                    if (xhr.readyState == XMLHttpRequest.DONE) {
                    var jsonResponse = JSON.parse(xhr.responseText);
                    if(jsonResponse.status == '1') {
                        location.reload(true);
                    }
                    else{
                        alert('Something went wrong as per response from the server is : ' + xhr.responseText);
                        location.reload(true);
                    }
                }
            }
            xhr.open('POST', 'http://' +  addr + ':' + port + '/api/configureServer/' + serverName, true);
            xhr.setRequestHeader("Content-Type", "application/json");

            console.log('payload:' + JSON.stringify(payload));
            xhr.send(JSON.stringify(payload));
    }

    /////////////////////////////////////////////////////////
    // Loading Dialog
    /////////////////////////////////////////////////////////
    function showLoadingDialog(){
        var options = {
             theme:"sk-falding-circle",
             message:'A team of highly trained monkeys has been dispatched to resolve your request.',
             textColor:"white"
        };

        HoldOn.open(options);
    }

    /////////////////////////////////////////////////////////
    // CheckExclusiveRights Function, Lock dialog, HideLock Dialog
    /////////////////////////////////////////////////////////
    function checkExclusiveRights(addr, port){
    /*
        Checks if actual user has exclusive rights to perform actions.
        Called on body load
        Called always when some action is performed -> TODO to implement
    */
        // Check lock
        var isLockedByCurrentUser = false;
        lockFromServer = _callLock(addr, port, getCookie('userName'));

        if (lockFromServer.toLowerCase() == getCookie('userName').toLowerCase()){
            // Is locked by current user
            isLockedByCurrentUser = true;
        }
        else{
            // Is locked by somebody else and lock dialog has to be shown
            showLockDialog(addr, port, lockFromServer);
        }

        return isLockedByCurrentUser;
    }

    function _callLock(addr, port, uid){
        var xhr = new XMLHttpRequest();
        var lock = '';

        xhr.open('POST', 'http://' +  addr + ':' + port + '/api/lock/' + uid, false);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send();

        var jsonResponse = JSON.parse(xhr.responseText);
        if(jsonResponse.status == '1') {
            console.log('Lock for this session has been created.')
            lock = jsonResponse.uid;
        }
        else{
            console.log('Some lock already exists ' + jsonResponse.uid)
            lock = jsonResponse.uid;
        }

        return lock;
    }

    function _obtainLock(addr, port){
        var xhr = new XMLHttpRequest();

        xhr.open('POST', 'http://' +  addr + ':' + port + '/api/unlock', false);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send();

        var jsonResponse = JSON.parse(xhr.responseText);
        if(jsonResponse.status == '1') {
            console.log('Unlocked')
            // and lock by new user
            _callLock(addr, port, getCookie('userName'));
        }

    }

    function _callUnlock(addr, port){
        var xhr = new XMLHttpRequest();

        xhr.open('POST', 'http://' +  addr + ':' + port + '/api/unlock', false);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send();

        var jsonResponse = JSON.parse(xhr.responseText);
        if(jsonResponse.status == '1') {
            console.log('Unlocked')
        }
    }

    function showLockDialog(addr, port, uid){
        var options = {
             theme:"custom",
             content:"",
             message:'<img style="width:80px;" src="http://' + addr + ':' + port + '/static/img/lock.png"><br>Deployer is currently locked by user "' + uid + '".<br> <input type="button" class="btn btn-danger btn-sm" value="Obtain exclusive rights" onclick="_obtainLock(\'' +  addr + '\',\'' + port + '\');HoldOn.close();"> | <input type="button" class="btn btn-primary btn-sm" value="Logout" onclick="logout(\'' +  addr + '\',\'' + port + '\');">',
             textColor:"white"
        };

        HoldOn.open(options);
    }

    function hideLockDialog(){
        HoldOn.close();
    }

    /////////////////////////////////////////////////////////
    // Create New Server - New Modal from boostrap
    /////////////////////////////////////////////////////////
    function showCreateNewServerDialog(addr, port){
        // Show Modal
        $('#createNewServerDialog').modal('show');
        // Perform action on positive button
        $('.createNewServerFormSaver').on('click', function () {
            var serverName = $("#serverName").val();

            var specials=/[^a-zA-Z0-9]/;

            if(serverName !== null && serverName !== '' && !specials.test(serverName)) {
                $("#createNewServerDialog").modal('hide');
                showLoadingDialog()
                _callCreateServer(addr, port , serverName)
            }
            else{
                document.getElementById('serverName').style.borderColor='#F51313';
            }
        });

     }

    function _callCreateServer(addr, port, serverName){
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                var jsonResponse = JSON.parse(xhr.responseText);
                if(jsonResponse.status == '1') {
                    location.reload(true);
                }
                else{
                    alert('Something went wrong as per response from the server is : ' + xhr.responseText);
                    location.reload(true);
                }
            }
        }
        xhr.open('POST', 'http://' +  addr + ':' + port + '/api/createServer/' + serverName, true);
        xhr.send(null);
     }

    function logout(addr, port){
        eraseCookie('authenticated');
        eraseCookie('userName');
        _callUnlock(addr, port)
        location.reload(true);
    }