    function _callAuthUser(addr, port){
        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function() {
                if (xhr.readyState == XMLHttpRequest.DONE) {
                var jsonResponse = JSON.parse(xhr.responseText);
                if(jsonResponse.status == '1') {
                    setCookie('authenticated', 'True', 7);
                    setCookie('userName', document.getElementById('userName').value, 7);
                    location.reload(true);
                }
                else{
                    eraseCookie('authenticated');
                    eraseCookie('userName');
                    alert('Something went wrong as per response from the server is : ' + xhr.responseText);
                    location.reload(true);
                }
            }
        }

        xhr.open('POST', 'http://' +  addr + ':' + port + '/api/authUser', true);
        xhr.setRequestHeader("Content-Type", "application/json");

        // construct json here and pass to send
        var $items = $('#userName, #password ')
        var payload = {}
        $items.each(function() {
            payload[this.id] = $(this).val()
        })

        // if payload contains password and we can hash it
        if(payload['password']){
            hashedPassword = MD5(payload['password']);
            payload['password'] = hashedPassword;
        }        //
        xhr.send(JSON.stringify(payload));
    }
