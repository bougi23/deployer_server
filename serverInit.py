import os
from multiprocessing import Process

from Models.Configuration import Configuration
from Models.Deployer import Deployer

c = Configuration()
deployer = Deployer().getApp()

if __name__ == '__main__':
    # set the environment variable
    envVar = '-Dops.environment=' + c.getEnvironmentName()
    os.environ["JVM_ARGS"] = envVar

    ipAddress = str(c.getServerIpAddress())
    port = int(c.getServerPort())

    # INIT server
    server = Process(target=deployer.run(host=ipAddress, port=port, threaded=True))
    server.start()
