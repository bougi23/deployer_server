# Deployer (server)

Deployer (server) is a [Python](https://www.python.org/) tool originally used for autodeployment of application package for [WebSphere Liberty Profile 17+](https://developer.ibm.com/wasdev/websphere-liberty/) and [JBOSS wildfly](http://wildfly.org/) (in current version 0.2 not supported yet :) ).

  - Server Administration (creation, configuration, removing)
  - Deployment of application package
  - Monitoring of server/application status
  - Simple REST API
  - .... and more :)

# New Features in version 0.2!

  - Support of Deployment package
  - So on ... [TODO]


### Tech

Deployer uses a number of open source projects to work properly:

* [Flask] - Microframework for Python based on Werkzeug and Jinja 2
* [jQuery] - duh
* [SQLite3] - Database engine

And of course Deployer itself is open source with a [public repository][deployer_repo]
 on BitBucket.

### Installation

Deployer requires [Python](https://www.python.org/) v3.6.0+ to run and some dependencies.
* [aniso8601] - version 2.0.0
* [certifi] - version 2018.1.18
* [chardet] - version 3.0.4
* [click] - version 6.7
* [Flask] - version 0.12.2
* [Flask-Jsonpify] - version 1.5.0
* [Flask-RESTful] - version 0.3.6
* [Flask-SQLAlchemy] - version 2.3.2
* [idna] - version 2.6
* [itsdangerous] - version 0.24
* [Jinja2] - version 2.10
* [MarkupSafe] - version 1.0
* [pytz] - version 2018.3
* [requests] - version 2.18.4
* [six] - version 1.11.0
* [SQLAlchemy] - version 1.2.2
* [urllib3] - version 1.22
* [Werkzeug] - version 0.14.1
* [zip] - version 0.0.1

1, Install Python by downloading from [Python](https://www.python.org/) site.
- Make sure that python folder is in Windows PATH and also ${PYTHON}\scripts\ folder is in Windows PATH as well

2, Install dependencies form requirements.txt
```sh
$ cd deployer
$ pip install -r requirements.txt
```

3, Database installation (if needed)
```sh
$ sqlite3 database.db < 0_schema_init.sql
```

### Configuration

Deployer configuration is stored in ${deployer}/config/**config.json** file.

`serverIpAddress` - Public IP address of server where deployer runs

`serverPort` - Port where deployer will be listening on

`applicationServerPath` - Root path to application server. Could be WebShere Liberty Profile or JBOSS wildfly

`tempPath` - Temporary folder used by deployer as temporary storage of deployment packages. By default it is ./_TEMP_/

`debug` - Extended logging [True/False]

`environmentName` - distinguished name for environemnt included in package configuration i.e. PRG_TEST

`mode` - Deployer supports WLP mode for Websphere Liberty Profile and WILDFLY for JBOSS wildfly [wlp/wildfly]


### First run

TODO

### Run as Windows Service

TODO

## XXX deployment package specifics in WLP

### Configure an encryption key for wlp mode
For XXX deployment package needs we have to create a new file with the encryption key.

Create a file `$WLP_HOME/usr/shared/config/ops/encryption_key.xml` with the following content:

    <?xml version="1.0" encoding="UTF-8"?>
    <server>
        <variable name="wlp.password.encryption.key" value="mySecretKey" />
    </server>


### Configure fake ldap file
For XXX deployment package needs we have to create a new file for LDAP. This file will be fake one.

Create a file `$WLP_HOME/usr/shared/config/ops/ldap.xml` with following EMPTY content:

    <?xml version="1.0" encoding="UTF-8"?>
    <server>
        <!-- I am just empty config file -->
    </server>

### Deployment Package structure for wlp mode

The structure of deployment package should be like following example:

```
package.zip
│
└───apps
│   │   application.war
│
└───config
│   └───ENVIRONMENT1
│   │   │   ... configuration files for ENV1
│   │
│   └───ENVIRONMENT2
│       │   ... configuration files for ENV1
│
└───resources
│   └───ENVIRONMENT1
│   │   │   ... resource files for ENV1
│   │
│   └───ENVIRONMENT2
│       │   ... resource files for ENV1
│
│   bootstrap.properties
│   jvm.options
│   server.env
│   server.xml
│
```

### Deployment Package structure for wildfly mode

TODO

### Todos

 - All above !!!
 - Some TODOS
 

License
----

MIT


[//]: # (These are reference links used in the body - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)
[//]: # (Good editor is https://dillinger.io/)


   [deployer_repo]: <https://bitbucket.org/bougi23/deployer_server>
